<?php

namespace Mvc\ModuleManager;

use Mvc\ModuleManager\SplClassLoader;

/**
 * Module manager
 */
class ModuleManager
{
	/**
     * @var array An array of Module classes of loaded modules
     */
    protected $loadedModules = array();

    /**
     * True if modules have already been loaded
     *
     * @var bool
     */
    protected $modulesAreLoaded = false;

    /**
     * modules
     *
     * @var array|Traversable
     */
    protected $configModules = array();

    /**
     * Constructor
     *
     * @param  array|Traversable $modules
     * @param  EventManagerInterface $eventManager
     */
    public function __construct($configModules)
    {
        $this->setConfigModules($configModules);
    }

    /**
     * Get the array of module names that this manager should load.
     *
     * @return array
     */
    public function getConfigModules()
    {
        return $this->configModules;
    }

    /**
     * Set an array or Traversable of module names that this module manager should load.
     *
     * @param  mixed $modules array or Traversable of module names
     * @throws Exception\InvalidArgumentException
     * @return ModuleManager
     */
    public function setConfigModules($configModules)
    {
        if (is_array($configModules) || $configModules instanceof Traversable) {
            $this->configModules = $configModules;
        } else {
            throw new Exception\InvalidArgumentException(sprintf(
                'Parameter to %s\'s %s method must be an array or implement the Traversable interface',
                __CLASS__, __METHOD__
            ));
        }
        return $this;
    }

    /**
     * Load the provided modules.
     *
     * @triggers loadModules
     * @triggers loadModules.post
     * @return   ModuleManager
     */
    public function loadModules()
    {
        if (true === $this->modulesAreLoaded) {
            return $this;
        }

        $configModules = $this->getConfigModules();
        $i = 0;
        while($i < count($configModules['modules'])) {
            $blogName = $configModules['modules'][$i];
            $splAutoLoader      = new SplClassLoader($blogName, $blogName."/src");
            $splAutoLoader->register();
            $i++;
        }

        return $this;
    }
}