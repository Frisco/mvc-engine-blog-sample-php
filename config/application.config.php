<?php
return array(
    'modules' => array(
        'Blog',
        'Cms'
    ),

    'module_listener_options' => array(
        'module_path' => './',
        'config_module_path' => 'config/config.module.php'
    ),
);